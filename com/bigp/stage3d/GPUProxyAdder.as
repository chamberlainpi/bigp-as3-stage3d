package com.bigp.stage3d {
	import flash.display.Bitmap;
	import flash.display.BitmapData;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUProxyAdder extends GPUProxy_Base {
		public function GPUProxyAdder() {
			super();
		}
		
		public function textureFromAsset(pPNGClass:Class, pName:String=null ):GPUTexture {
			GPU._CAN_INSTANTIATE = true;
			var theTexture:GPUTexture = new GPUTexture( pName );
			var theBitmap:BitmapData =  Bitmap(new pPNGClass).bitmapData;
			theTexture.init( theBitmap );
			
			return theTexture;
		}
	}
}