package com.bigp.stage3d {
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import avm2.intrinsics.memory.*;
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUStateSimple extends GPUState {
		public function GPUStateSimple() {
			super();
		}
		
		public override function prepareShader():void {
			var view:Rectangle = GPU._INSTANCE._config.viewRect;
			constantsFragment = new <Number>[1, 1, 1, 1];
			assembleProgram([
				//"mov vt0,  va0;",
				"add vt0, va0, vc[va2.x];",
				"mul op, vt0, vc0;",
				"mov v0, va1;"
			],[
				"mul oc, fc0, v0;"
			], true );
			
			setCachedAttribute( GPUAttribute.TYPE_04_POSITION_XY, updatePosition );
			setCachedAttribute( GPUAttribute.TYPE_06_COLOR_MULTIPLIER, updateColorMultiplier );
			setCachedAttribute( GPUAttribute.TYPE_07_COLOR_OFFSET, updateColorOffset );
			setCachedAttribute( GPUAttribute.TYPE_00_CONSTANT, updateConstantIndex00 );
		}
		
		[Inline]
		private final function updatePosition( pRenderer:GPUProxyRenderer, pMesh:GPUMesh, pStack:GPUStack, pPositions:GPUAttribute ):void {
			var theSin:Number = Math.sin(pMesh._rotation);
			var theCos:Number = Math.cos(pMesh._rotation);
			var thePivotX:Number = pMesh._pivotX;
			var thePivotY:Number = pMesh._pivotY;
			var theXStart:Number = pMesh._x;
			var theYStart:Number = pMesh._y;
			var theScaleX:Number = pMesh._scaleX;
			var theScaleY:Number = pMesh._scaleY;
			var theVertexPositions:Vector.<Number> = pMesh._vertexPositions._points;
			var theBytes:ByteArray = pStack._bytesShared;
			var theNumFields:int = pPositions._numOfFields;
			var theStartID:int = pMesh._stackStartIndex;
			var theAddress:uint = pPositions._bytesSharedStart + ((theStartID * theNumFields) << 2);
			
			CONFIG::regularmem {
				theBytes.position = theAddress;
			}
			
			//Update stack's position values:
			for (var j:int = 0, jLen:int = pMesh._numVertices; j < jLen; j++) {
				var theVertexID:int = j << 1;
				var pointX:Number = (theScaleX * theVertexPositions[theVertexID]) - thePivotX;
				var pointY:Number = (theScaleY * theVertexPositions[++theVertexID]) - thePivotY;
				var transformedX:Number = theXStart + (theCos * pointX) - (theSin * pointY);
				var transformedY:Number = theYStart + (theSin * pointX) + (theCos * pointY);
				
				CONFIG::domainmem {
					sf32( transformedX, theAddress ); theAddress += 4;
					sf32( transformedY, theAddress ); theAddress += 4;
				}
				
				CONFIG::regularmem {
					theBytes.writeFloat( transformedX );
					theBytes.writeFloat( transformedY );
				}
			}
		}
		
		[Inline]
		private final function updateColorMultiplier(pRenderer:GPUProxyRenderer, pMesh:GPUMesh, pStack:GPUStack, pColorMultipliers:GPUAttribute):void {
			var color:GPUColor, r:Number, g:Number, b:Number, a:Number;
			var theBytes:ByteArray = pStack._bytesShared;
			var offset:int = pMesh._stackStartIndex;
			var j:int = 0, jLen:int = pMesh._numVertices;
			var theNumFields:int = 4;
			var theAddress:uint = pColorMultipliers._bytesSharedStart + ((offset * theNumFields) << 2);
			
			CONFIG::regularmem {
				theBytes.position = theAddress;
			}
			
			//Update stack's position values:
			color = pMesh._colorMultiplier;
			r = color.r;
			g = color.g;
			b = color.b;
			a = color.a;
			
			for (j = 0; j < jLen; j++) {
				CONFIG::domainmem {
					sf32( r, theAddress ); theAddress += 4;
					sf32( g, theAddress ); theAddress += 4;
					sf32( b, theAddress ); theAddress += 4;
					sf32( a, theAddress ); theAddress += 4;
				}
				CONFIG::regularmem {
					theBytes.writeFloat( r );
					theBytes.writeFloat( g );
					theBytes.writeFloat( b );
					theBytes.writeFloat( a );
				}
			}
		}
		
		[Inline]
		private final function updateColorOffset(pRenderer:GPUProxyRenderer, pMesh:GPUMesh, pStack:GPUStack, pColorOffsets:GPUAttribute):void {
			var color:GPUColor, r:Number, g:Number, b:Number, a:Number;
			var theBytes:ByteArray = pStack._bytesShared;
			var offset:int = pMesh._stackStartIndex;
			var j:int = 0, jLen:int = pMesh._numVertices;
			var theNumFields:int = 4;
			var theAddress:uint = pColorOffsets._bytesSharedStart + ((offset * theNumFields) << 2);
			
			CONFIG::regularmem {
				theBytes.position = theAddress;
			}
			
			//Update stack's position values:
			color = pMesh._colorOffset;
			r = color.r;
			g = color.g;
			b = color.b;
			a = color.a;
			
			for (j = 0; j < jLen; j++) {
				CONFIG::domainmem {
					sf32( r, theAddress ); theAddress += 4;
					sf32( g, theAddress ); theAddress += 4;
					sf32( b, theAddress ); theAddress += 4;
					sf32( a, theAddress ); theAddress += 4;
				}
				
				CONFIG::regularmem {
					theBytes.writeFloat( r );
					theBytes.writeFloat( g );
					theBytes.writeFloat( b );
					theBytes.writeFloat( a );
				}
			}
		}
		
		[Inline]
		private final function updateConstantIndex00(pRenderer:GPUProxyRenderer, pMesh:GPUMesh, pStack:GPUStack, pFastPositions:GPUAttribute):void {
			var j:int = 0, jLen:int = pMesh._numVertices;
			var theAddress:int = pFastPositions._bytesSharedStart + ((pMesh._stackStartIndex * pFastPositions._numOfFields) << 2);
			var theBytes:ByteArray = pStack._bytesShared, theConstantID:int;
			
			if (pMesh.constant00) {
				theConstantID = pMesh.constant00.index;
			} else {
				theConstantID = pFastPositions.defaultValue;
			}
			
			CONFIG::regularmem {
				theBytes.position = theAddress;
			}
			
			for (j = 0; j < jLen; j++) {
				CONFIG::domainmem {
					sf32( theConstantID, theAddress ); theAddress += 4;
					sf32( theConstantID, theAddress ); theAddress += 4;
					sf32( theConstantID, theAddress ); theAddress += 4;
					sf32( theConstantID, theAddress ); theAddress += 4;
				}
				
				CONFIG::regularmem {
					theBytes.writeFloat( theConstantID );
					theBytes.writeFloat( theConstantID );
					theBytes.writeFloat( theConstantID );
					theBytes.writeFloat( theConstantID );
				}
			}
		}
	}
}