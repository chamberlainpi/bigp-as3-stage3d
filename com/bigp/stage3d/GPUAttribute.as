package com.bigp.stage3d {
	import flash.display3D.Context3D;
	import flash.display3D.VertexBuffer3D;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUAttribute extends GPU_Disposable implements IGPUHandleLostContext {
		private static var _TYPE_ID:int = 1;
		public static var TYPE_00_CONSTANT:int = _TYPE_ID;
		public static var TYPE_01_CONSTANT:int = _TYPE_ID <<= 1;
		public static var TYPE_02_CONSTANT:int = _TYPE_ID <<= 1;
		public static var TYPE_03_CONSTANT:int = _TYPE_ID <<= 1;
		public static var TYPE_04_POSITION_XY:int = _TYPE_ID <<= 1;
		public static var TYPE_05_TEXTURE_UV:int = _TYPE_ID <<= 1;
		public static var TYPE_06_COLOR_MULTIPLIER:int = _TYPE_ID <<= 1;
		public static var TYPE_07_COLOR_OFFSET:int = _TYPE_ID <<= 1;
		public static var TYPE_08_NORMAL:int = _TYPE_ID <<= 1;
		public static var TYPE_09_TIME_INTERPOLATE:int = _TYPE_ID <<= 1;
		public static var TYPE_10_VALUES_START:int = _TYPE_ID <<= 1;
		public static var TYPE_11_VALUES_END:int = _TYPE_ID <<= 1;
		
		public var isActive:Boolean = true;
		public var defaultValue:Number = 0;
		
		internal var _type:int = 0;
		internal var _numOfFields:int = 0;
		internal var _bytesSharedStart:int;
		internal var _bytesRequired:int;
		internal var _stack:GPUStack;
		internal var _vertex:VertexBuffer3D;
		internal var _isDirty:Boolean = true;
		
		public function GPUAttribute(pNumOfFields:int) {
			super(false);
			
			_numOfFields = pNumOfFields;
		}
		
		public static function generateNewType():int {
			return _TYPE_ID <<= 1;
		}
		
		public override function dispose():void {
			if (!_vertex) return;
			_vertex.dispose();
			_vertex = null;
			_stack = null;
			
			super.dispose();
		}
		
		public function setActiveAndDefault( pActive:Boolean, pDefaultValue:Number = 0 ):GPUAttribute {
			isActive = pActive;
			defaultValue = pDefaultValue;
			return this;
		}
		
		/* INTERFACE com.bigp.stage3d.IGPUHandleLostContext */
		
		public function onContextCreated(pContext:Context3D):void {
			if (CONFIG::GPU_OFF) { return; }
			
			if (_vertex) {
				_vertex.dispose();
				_vertex = null;
			}
			
			if (_numOfFields == 0 || pContext==null) return;
			
			_vertex = pContext.createVertexBuffer(numVertices, _numOfFields);
			
			uploadRange(0, numVertices);
		}
		
		public function uploadRange( pMin:int, pMax:int ):void {
			var minBytes:int = _bytesSharedStart + ((pMin * _numOfFields) << 2);
			
			if (CONFIG::GPU_OFF) { return; }
			
			GPU._INSTANCE._numOfUploads++;
			GPU._INSTANCE._bytesUploaded += (pMax - pMin) << 2;
			_vertex.uploadFromByteArray( _stack._bytesShared, minBytes, pMin, pMax-pMin);
		}
		
		
		[Inline]
		public final function get numVertices():int { return _stack._numVertices; }
		
		public function get type():int { return _type; }
	}
}