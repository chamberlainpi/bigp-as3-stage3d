package com.bigp.stage3d {

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUColor {
		
		public var a:Number = 1;
		public var r:Number = 0;
		public var g:Number = 0;
		public var b:Number = 0;
		
		public function GPUColor() {
			
		}
		
		[Inline]
		public final function setRGBA(pR:Number, pG:Number, pB:Number, pA:Number):void {
			this.r = pR;
			this.g = pG;
			this.b = pB;
			this.a = pA;
		}
		
		[Inline]
		public final function setHex(pColor:uint):void {
			b = (pColor & 0xff) / 0xff;
			pColor >>>= 8;
			g = (pColor & 0xff) / 0xff;
			pColor >>>= 8;
			r = (pColor & 0xff) / 0xff;
			pColor >>>= 8;
			a = (pColor & 0xff) / 0xff;
			pColor >>>= 8;
		}
		
		public function getHex():uint {
			return (a * 0xff) << 24 | (r * 0xff) << 16 | (g * 0xff) << 8 | (b * 0xff) ;
		}
		
		public function toString():String {
			return "[GPUColor: " + [r, g, b, a].join(", ") + "]";
		}
	}
}