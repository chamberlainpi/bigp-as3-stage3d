package com.bigp.stage3d {
	import com.bigp.EnterFramer;
	import com.bigp.Lib;
	import com.bigp.utils.SignalUtils;
	import flash.automation.Configuration;
	import flash.display.Stage;
	import flash.display.Stage3D;
	import flash.display.StageDisplayState;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DBlendFactor;
	import flash.events.Event;
	import flash.geom.Rectangle;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPU extends GPU_Disposable {
		internal static const _DRIVER_INFO_DISPOSED:String = "Disposed";
		internal static var _CAN_INSTANTIATE:Boolean = false;
		internal static var _INSTANCE:GPU;
		public static var CLASS_TYPE:Class;
		
		private var _inited:Boolean = false;
		private var _stage:Stage;
		
		internal var _stage3D:Stage3D;
		internal var _context:Context3D;
		internal var _config:GPUConfig;
		internal var _proxyAdder:GPUProxyAdder;
		internal var _proxyRemover:GPUProxyRemover;
		internal var _proxyRenderer:GPUProxyRenderer;
		internal var _proxyInventory:GPUProxyInventory;
		internal var _defaultState:GPUState;
		internal var _state:GPUState;
		internal var _numOfRenders:int = 0;
		internal var _numOfUploads:int = 0;
		internal var _bytesUploaded:int = 0;
		
		public var whenInitialized:SignalUtils;
		public var whenContextLossHandled:SignalUtils;
		public var whenClear:SignalUtils;
		public var whenRender:SignalUtils;
		public var whenPresent:SignalUtils;
		public var whenResized:SignalUtils;
		public var backgroundColor:GPUColor;
		public var clearDepth:Number = 1;
		public var clearStencil:uint = 0;
		public var clearMask:uint = 4294967295;
		public var root:GPUContainerRoot;
		
		public static function getInstance():GPU {
			if (!_INSTANCE) {
				_CAN_INSTANTIATE = true;
				_INSTANCE = CLASS_TYPE != null ? new CLASS_TYPE() : new GPU();
				_INSTANCE.preinit();
			}
			
			return _INSTANCE;
		}
		
		public static function dispose(pRecreate:Boolean = true):void {
			var disposeFunc:Function = _INSTANCE._context.dispose;
			if(disposeFunc.length==0) {
				disposeFunc();
				if (pRecreate) {
					_INSTANCE.requestContext();
				}
			} else {
				disposeFunc.apply(null, [pRecreate]);
			}
		}
		
		public function GPU() { super(false); }
		
		private function preinit():void {
			whenInitialized = new SignalUtils();
			whenContextLossHandled = new SignalUtils();
			whenClear = new SignalUtils();
			whenRender = new SignalUtils();
			whenPresent = new SignalUtils();
			whenResized = new SignalUtils();
			backgroundColor = new GPUColor();
			
			_proxyInventory = new GPUProxyInventory();
			_proxyAdder = new GPUProxyAdder();
			_proxyRemover = new GPUProxyRemover();
			_proxyRenderer = new GPUProxyRenderer();
		}
		
		public function init( pStage:Stage=null, pConfig:GPUConfig=null ):GPU {
			if (_inited) throw new Error("GPU already initialized.");
			_inited = true;
			_stage = pStage || Lib.STAGE;
			_config = pConfig || new GPUConfig();
			_stage3D = _stage.stage3Ds[0];
			
			if (!_config.viewRect) {
				_config.viewRect = new Rectangle(0, 0, _stage.stageWidth, _stage.stageHeight);
				_config.isStageRect = true;
				
				_stage.addEventListener(Event.RESIZE, onStageResize);
			}
			
			_config.init();
			root = new GPUContainerRoot();
			
			_stage3D.addEventListener(Event.CONTEXT3D_CREATE, onContextCreated);
			requestContext();
			
			return this;
		}
		
		private function requestContext():void {
			if (_stage3D.requestContext3D.length == 1) {
				_stage3D.requestContext3D(_config.mode);
			} else {
				_stage3D.requestContext3D.apply(null, [_config.mode, _config.profile]);
			}
		}
		
		private function onStageResize(e:Event = null):void {
			_config.viewRect.width = Lib.WIDTH;
			_config.viewRect.height = Lib.HEIGHT;
			_config.inline_updateInverse();
			
			if (!_context) return;
			_context.configureBackBuffer( _config.viewRect.width, _config.viewRect.height, _config.antiAlias, _config.enableDepthAndStencil); //_config.wantsBestResolution
			
			whenResized.dispatch();
		}
		
		private function onContextCreated(e:Event):void {
			var oldContext:Context3D = _context;
			_context = _stage3D.context3D;
			onStageResize();
			//_context.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ZERO);
			_context.setBlendFactors(Context3DBlendFactor.SOURCE_ALPHA, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);
			//_context.setDepthTest( true, Context3DCompareMode.ALWAYS );
			_context.enableErrorChecking = CONFIG::debug;
			
			//Offset the view if the view-rect says so:
			_stage3D.x = _config.viewRect.x;
			_stage3D.y = _config.viewRect.y;
			
			if (!_defaultState) {
				_defaultState = new GPUStateSimple();
			}
			
			//Begin with the default state:
			state = _defaultState;
			
			if (oldContext) {
				whenContextLossHandled.dispatchParam(_context);
			} else {
				whenInitialized.dispatch();
				EnterFramer.signal.add( onUpdateFrame );
			}
		}
		
		private function onUpdateFrame():void {
			if (_context.driverInfo == _DRIVER_INFO_DISPOSED) {
				return; //Wait for context to be recreated...
			}
			
			_numOfUploads = _bytesUploaded = 0;
			
			_context.clear(
				backgroundColor.r,
				backgroundColor.g,
				backgroundColor.b,
				backgroundColor.a,
				clearDepth,
				clearStencil,
				clearMask
			);
			
			whenClear.dispatch();
			
			if (_state) {
				processState(_state);
			}
			
			_proxyRenderer.render();
			whenRender.dispatch();
			
			_context.present();
			whenPresent.dispatch();
			
			_numOfRenders++;
		}
		
		[Inline]
		private final function processState( pState:GPUState ):void {
			pState.updateConstants();
			pState.uploadConstants();
			
			if (pState._isDirty) {
				pState._isDirty = false;
				
				_context.setProgram( pState._program );
				pState.added();
			}
		}
		
		public static function get add():GPUProxyAdder { return _INSTANCE._proxyAdder; }
		public static function get remove():GPUProxyRemover { return _INSTANCE._proxyRemover; }
		public static function get renderer():GPUProxyRenderer { return _INSTANCE._proxyRenderer; }
		public static function get backgroundColor():uint { return _INSTANCE.backgroundColor.getHex(); }
		public static function set backgroundColor(pColor:uint):void { return _INSTANCE.backgroundColor.setHex(pColor); }
		public static function get numOfUploads():int { return _INSTANCE._numOfUploads; }
		public static function get bytesUploaded():int { return _INSTANCE._bytesUploaded; }
		public static function get context():Context3D { return _INSTANCE._context; }
		public static function get root():GPUContainerRoot { return _INSTANCE.root; }
		public static function get state():GPUState { return _INSTANCE._state; }
		public static function set state(value:GPUState):void { _INSTANCE.state = value; }
		
		public function get add():GPUProxyAdder { return _proxyAdder; }
		public function get remove():GPUProxyRemover { return _proxyRemover; }
		public function get defaultState():GPUState { return _defaultState; }
		public function set defaultState(value:GPUState):void { _defaultState = value; }
		public function get state():GPUState { return _state; }
		public function set state(value:GPUState):void {
			if (_state) {
				_state.removed();
			}
			
			_state = value;
			if (!_state) return;
			
			_state.added();
			_context.setProgram( _state._program );
		}
	}
}