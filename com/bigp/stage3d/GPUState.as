package com.bigp.stage3d {
	import com.adobe.utils.AGALMiniAssembler;
	import com.bigp.Lib;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Program3D;
	import flash.display3D.textures.TextureBase;
	import flash.geom.Matrix3D;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	import flash.utils.getTimer;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUState extends GPUProgram {
		private static const _SCRIPT_TIME_MAX:int = 1500;
		
		internal var _isInUse:Boolean = false;
		internal var _generatedConstants:Vector.<GPUConstant>;
		internal var _generatedConstantData:ByteArray;
		internal var _generatedConstantNextID:int = 0;
		internal var _cachedAttributes:Dictionary;
		internal var _currentStack:GPUStack;
		
		public var generatedConstantStartID:int = -1;
		public var matrix:Matrix3D;
		public var scissorRect:Rectangle;
		public var constantsVertex:Vector.<Number>;
		public var constantsFragment:Vector.<Number>;
		public var textureIndexOffset:int = 0;
		private var _textures:Array;
		
		public function GPUState() {
			super();
			
			GPU._INSTANCE.whenContextLossHandled.add(onContextCreated);
			GPU._INSTANCE.whenResized.add(onContextResized);
			_generatedConstants = new Vector.<GPUConstant>();
			_generatedConstantData = new ByteArray();
			_generatedConstantData.endian = Endian.LITTLE_ENDIAN;
			_cachedAttributes = new Dictionary();
			
			//Default Vertex constants:
			constantsVertex = new <Number>[0,0,0,1];
			
			prepareShader();
			generatedConstantStartID = constantsVertex ? int(constantsVertex.length / 4) + 1 : 0;
		}
		
		public override function dispose():void {
			if (!_program) return;
			
			if (_isInUse) {
				GPU.state = null;
			}
			
			_program.dispose();
			_shaderFragment.clear();
			_shaderVertex.clear();
			_generatedConstants.length = 0;
			_textures.length = 0;
			
			matrix = null;
			scissorRect = null;
			constantsVertex = null;
			constantsFragment = null;
			_textures = null;
			_generatedConstants = null;
			
			GPU._INSTANCE.whenContextLossHandled.remove(onContextCreated);
			GPU._INSTANCE.whenResized.remove(onContextResized);
			super.dispose();
		}
		
		public override function onContextCreated(pContext:Context3D):void {
			super.onContextCreated(pContext);
			onContextResized();
		}
		
		private function onContextResized():void {
			if (!constantsVertex) return;
			var theConfig:GPUConfig = GPU._INSTANCE._config;
			var view:Rectangle = theConfig._viewRectInverse;
			constantsVertex[0] = view.width;
			constantsVertex[1] = view.height;
		}
		
		public function setDefaultMatrix():void {
			matrix = new Matrix3D();
			matrix.identity();
			matrix.appendScale( 1 / Lib.WIDTH_HALF, -1 / Lib.HEIGHT_HALF, 1);
		}
		
		[Inline]
		protected final function setCachedAttribute( pType:int, pUpdateCallback:Function ):void {
			_cachedAttributes[pType] = new clsGPUCustomAttributeUpdater( pUpdateCallback );
		}
		
		public function generateConstant():GPUConstant {
			var theConstant:GPUConstant = new GPUConstant(_generatedConstantNextID);
			_generatedConstants[_generatedConstants.length] = theConstant;
			_generatedConstantNextID++;
			return theConstant;
		}
		
		public function generateConstantReset():void {
			_generatedConstants.length = 0;
			_generatedConstantData.length = 0;
			_generatedConstantNextID = generatedConstantStartID;
		}
		
		[Inline]
		public final function adjustMinMaxChanged( pMesh:GPUMesh, pStack:GPUStack ):void {
			if (pMesh._stackStartIndex > pStack.vertexChangedMax) {
				pStack.vertexChangedMax = pMesh._stackStartIndex + pMesh._numVertices; // # + totalVerticesOfThisMesh
			}
			
			if (pMesh._stackStartIndex < pStack.vertexChangedMin) {
				pStack.vertexChangedMin = pMesh._stackStartIndex;
			}
		}
		
		//OVERRIDE
		public function prepareShader():void {}
		public function updateConstants():void { }
		
		[Inline]
		internal final function iterateMeshes( pRenderer:GPUProxyRenderer, pCurrentContainer:GPUContainer ):GPUContainer {
			var startTime:int = getTimer();
			
			var theFilter:GPUFilter = pCurrentContainer._filter;
			if (theFilter) {
				theFilter.preRender();
			}
			
			do {
				if (pCurrentContainer.gpuState) {
					if (pRenderer._currentStack) {
						pRenderer.drawCurrentStack();
					}
					pCurrentContainer = pCurrentContainer.gpuState.iterateMeshes( pRenderer, pCurrentContainer._linkNext );
				}
				
				if (!(pCurrentContainer is GPUMesh)) {
					continue;
				}
				
				var theMesh:GPUMesh = GPUMesh(pCurrentContainer);
				var theStack:GPUStack = theMesh._stack;
				
				if (pRenderer._currentStack != theStack) {
					if (pRenderer._currentStack) {
						pRenderer.drawCurrentStack();
					}
					
					pRenderer._indexAddress = 0; //Reset the indices to zero
					
					pRenderer._currentStack = theStack;
					theStack._numTriangles = 0;
					theStack.vertexChangedMax = -1;
					theStack.vertexChangedMin = theStack._numVertices + 1;
					
					cacheAttributesOfStack( theStack );
				}
				
				
				//Update mesh:
				if (theMesh._dirtyFlags > 0) {
				
					theStack._isDirtyAttributes = true;
					
					for (var attrType:int in _cachedAttributes) {
						if ((theMesh._dirtyFlags & attrType) > 0) {
							var theCached:clsGPUCustomAttributeUpdater = clsGPUCustomAttributeUpdater(_cachedAttributes[attrType]);
							theCached.cachedAttribute._isDirty = true;
							theCached.callbackUpdate( pRenderer, theMesh, theStack, theCached.cachedAttribute );
							theMesh._dirtyFlags ^= attrType;
						}
					}
					
					adjustMinMaxChanged( theMesh, theStack );
					pRenderer.appendMeshToIndexBytes( theMesh );
					
					theMesh._dirtyFlags = 0;
				}
				
			} while (pCurrentContainer!=null && (pCurrentContainer = pCurrentContainer._linkNext))
			
			//Draw the last stack iterated (if any)
			if (pRenderer._currentStack) {
				pRenderer.drawCurrentStack();
			}
			
			if (theFilter) {
				theFilter.renderTexture();
				theFilter.postRender();
				
				context.setProgram(_program);
			}
			
			return pCurrentContainer;
		}
		
		[Inline]
		private final function cacheAttributesOfStack( pStack:GPUStack ):void {
			if (_currentStack === pStack) return;
			
			for (var attrType:int in _cachedAttributes) {
				var theCached:clsGPUCustomAttributeUpdater = clsGPUCustomAttributeUpdater(_cachedAttributes[attrType]);
				theCached.cachedAttribute = pStack.getAttributeByType( attrType );
			}
			
			_currentStack = pStack;
		}
		
		[Inline]
		public final function uploadConstants():void {
			if (CONFIG::GPU_OFF) { return; }
			
			var theGPU:GPU = GPU._INSTANCE;
			var constantVertexOffset:int = 0;
			if (matrix) {
				context.setProgramConstantsFromMatrix( Context3DProgramType.VERTEX, 0, matrix, true );
				constantVertexOffset += 4;
			}
			
			if (_generatedConstants && _generatedConstants.length > 0) {
				_generatedConstantData.position = 0;
				for (var i:int=0, iLen:int=_generatedConstants.length; i<iLen; i++) {
					var theConst:GPUConstant = _generatedConstants[i];
					_generatedConstantData.writeFloat( theConst.x );
					_generatedConstantData.writeFloat( theConst.y );
					_generatedConstantData.writeFloat( theConst.z );
					_generatedConstantData.writeFloat( theConst.w );
				}
				
				theGPU._numOfUploads++;
				theGPU._bytesUploaded += _generatedConstantData.length;
				context["setProgramConstantsFromByteArray"]( Context3DProgramType.VERTEX, generatedConstantStartID, _generatedConstants.length, _generatedConstantData, 0);
			}
			
			if (constantsVertex && constantsVertex.length > 0) {
				theGPU._numOfUploads++;
				theGPU._bytesUploaded += constantsVertex.length << 2;
				context.setProgramConstantsFromVector( Context3DProgramType.VERTEX, constantVertexOffset, constantsVertex );
			}
			
			if (constantsFragment && constantsFragment.length > 0) {
				theGPU._numOfUploads++;
				theGPU._bytesUploaded += constantsFragment.length << 2;
				context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 0, constantsFragment );
			}
		}
		
		public function added():void {
			_isInUse = true;
			if (_textures && _textures.length > 0) {
				for (var i:int=0, iLen:int=_textures.length; i<iLen; i++) {
					var theGPUTexture:GPUTexture = _textures[i];
					var theTexture:TextureBase = theGPUTexture.texture;
					context.setTextureAt(int(i + textureIndexOffset), theTexture);
				}
			}
			
			if(scissorRect) {
				context.setScissorRectangle(scissorRect); //config.viewRectOrigin);
			}
		}
		
		public function removed():void {
			_isInUse = false;
			
			//Nullify the textures used in this state:
			if (_textures && _textures.length > 0) {
				for (var i:int=0, iLen:int=_textures.length; i<iLen; i++) {
					var theTexture:GPUTexture = _textures[i];
					context.setTextureAt(int(i + textureIndexOffset), null);
				}
			}
		}
		
		public static function get gpuConfig():GPUConfig { return GPU.getInstance()._config; }
		public static function get context():Context3D { return GPU.context; }
		
		public function get textures():Array { return _textures; }
		public function set textures(value:Array):void {
			_textures = value;
			
			_isDirty = true;
		}
		
		public function get generatedConstants():Vector.<GPUConstant> { return _generatedConstants; }
	}
}

import com.bigp.stage3d.GPUAttribute;

internal class clsGPUCustomAttributeUpdater {
	public var callbackUpdate:Function;
	public var cachedAttribute:GPUAttribute;
	
	public function clsGPUCustomAttributeUpdater( pCallbackUpdate:Function ) {
		callbackUpdate = pCallbackUpdate;
	}
}