package com.bigp.stage3d {
	import com.adobe.utils.AGALMiniAssembler;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Program3D;
	import flash.utils.ByteArray;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUProgram extends GPU_Disposable implements IGPUHandleLostContext {
		
		internal var _isDirty:Boolean = true;
		internal var _program:Program3D;
		internal var _shaderVertex:ByteArray;
		internal var _shaderFragment:ByteArray;
		
		public function GPUProgram() {
			super(true);
			
		}
		
		/* INTERFACE com.bigp.stage3d.IGPUHandleLostContext */
		
		public function onContextCreated(pContext:Context3D):void {
			_isDirty = true;
			
			if (CONFIG::GPU_OFF) {
				return;
			}
			
			if (!_shaderVertex || _shaderVertex.length == 0) return;
			
			_program = pContext.createProgram();
			_program.upload( _shaderVertex, _shaderFragment );
		}
		
		[Inline]
		protected final function assembleProgram( pVertexStr:Object, pFragmentStr:Object, pAndUpload:Boolean ):void {
			if (pVertexStr is Array) pVertexStr = (pVertexStr as Array).join("\n");
			if (pFragmentStr is Array) pFragmentStr = (pFragmentStr as Array).join("\n");
			
			var isDebuggable:Boolean = false;
			var assVertex:AGALMiniAssembler = new AGALMiniAssembler(isDebuggable);
			assVertex.assemble(Context3DProgramType.VERTEX, String(pVertexStr));
			
			var assFragment:AGALMiniAssembler = new AGALMiniAssembler(isDebuggable);
			assFragment.assemble(Context3DProgramType.FRAGMENT, String(pFragmentStr));
			
			_shaderVertex = assVertex.agalcode;
			_shaderFragment = assFragment.agalcode;
			
			if (pAndUpload) onContextCreated( context );
		}
	}
}