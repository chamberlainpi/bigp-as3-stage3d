package com.bigp.stage3d {
	import com.bigp.utils.ClassUtils;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPU_IdObject {
		private static var _STACK_ID:int = 0;
		internal var _id:int = 0;
		
		
		public function GPU_IdObject() {
			_id = ++_STACK_ID;
		}
		
		public function toString():String {
			return "[" + ClassUtils.nameOf(this) + "#" + _id + "]";
		}
	}
}