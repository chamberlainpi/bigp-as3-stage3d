package com.bigp.stage3d {

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUConstant {
		
		public var x:Number = 0;
		public var y:Number = 0;
		public var z:Number = 0;
		public var w:Number = 0;
		public var index:int = 0;
		
		public function GPUConstant(pIndex:int) {
			index = pIndex;
		}
	}
}