package com.bigp.stage3d {

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUMesh extends GPUContainer {
		
		internal var _x:Number = 0;
		internal var _y:Number = 0;
		internal var _pivotX:Number = 0;
		internal var _pivotY:Number = 0;
		internal var _scaleX:Number = 1;
		internal var _scaleY:Number = 1;
		internal var _rotation:Number = 0;
		internal var _visible:Boolean = true;
		internal var _vertexPositions:GPUPositionVertices;
		internal var _colorOffset:GPUColor;
		internal var _colorMultiplier:GPUColor;
		
		internal var _stack:GPUStack;
		internal var _stackStartIndex:int = 0;
		internal var _indexingPattern:Vector.<int>;
		internal var _numVertices:int = 0;
		internal var _dirtyFlags:int = 0;
		internal var _constant00:GPUConstant;
		internal var _constant01:GPUConstant;
		internal var _constant02:GPUConstant;
		internal var _constant03:GPUConstant;
		
		public var isSharedVertices:Boolean = false;
		
		public function GPUMesh(  ) {
			super();
		}
		
		public function buildMesh(pNumOfVertices:int, pIndexingPattern:Vector.<int>, pStack:GPUStack, pSharedVertices:GPUPositionVertices=null):void {
			_numVertices = pNumOfVertices;
			_indexingPattern = pIndexingPattern;
			_stack = pStack;
			_stackStartIndex = _stack.nextAvailableVertexID;
			_stack.nextAvailableVertexID += _numVertices;
			
			if ((_stackStartIndex + _numVertices) > _stack._numVertices) {
				throw new Error("Cannot create mesh, as it overflows the amount of vertices used in the GPUStack. Increase size, or use less meshes / vertices.");
			}
			
			if (pSharedVertices) {
				_vertexPositions = pSharedVertices;
				isSharedVertices = true;
			} else {
				//Fixed size of vertex-positions (for it's normal scale / not offset)
				_vertexPositions = new GPUPositionVertices( new Vector.<Number>( _numVertices << 1, true) );
				isSharedVertices = false;
			}
		}
		
		public override function dispose():void {
			if (!_stack) return;
			
			//_stack.releaseVertices( _stackStartIndex, _numVertices ); // ???????????????
			
			_linkNext = null;
			_stack = null;
			_indexingPattern = null;
			_vertexPositions = null;
			_colorMultiplier = null;
			_colorOffset = null;
			
			super.dispose();
		}
		
		[Inline]
		public final function setPositionAndRotation( pX:Number, pY:Number, pRotation:Number ):void {
			_x = pX;
			_y = pY;
			_rotation = pRotation;
			_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
		}
		
		public function get x():Number { return _x; }
		public function set x(value:Number):void {
			if (_x == value) return;
			_x = value;
			
			_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
		}
		
		public function get y():Number { return _y; }
		public function set y(value:Number):void {
			if (_y == value) return;
			_y = value;
			
			_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
		}
		
		public function get rotation():Number { return _rotation; }
		public function set rotation(value:Number):void {
			_rotation = value;
			
			_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
		}
		
		public function get pivotX():Number { return _pivotX; }
		public function set pivotX(value:Number):void {
			if (_pivotX == value) return;
			_pivotX = value;
			
			_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
		}
		
		public function get pivotY():Number { return _pivotY; }
		public function set pivotY(value:Number):void {
			if (_pivotY == value) return;
			_pivotY = value;
			
			_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
		}
		
		public function get scaleX():Number { return _scaleX; }
		public function set scaleX(value:Number):void {
			if (_scaleX == value) return;
			_scaleX = value;
			
			_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
		}
		
		public function get scaleY():Number { return _scaleY; }
		public function set scaleY(value:Number):void {
			if (_scaleY == value) return;
			_scaleY = value;
			
			_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
		}
		
		public function get visible():Boolean { return _visible; }
		public function set visible(value:Boolean):void {
			if (_visible == value) return;
			_visible = value;
			
			GPU.root._isHierarchyDirty = true;
			//_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY; //Not really "true" but triggers a redraw when it reappears at least.
		}
		
		public function setVertex( pID:int, pX:Number, pY:Number ):void {
			var id:int = pID << 1; //Double the index so that x and y's can be merged in same vector:
			_vertexPositions._points[id] = pX;
			_vertexPositions._points[int(id + 1)] = pY;
			_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
		}
		
		[Inline]
		public static function invalidatePositions( pMeshes:Vector.<GPUMesh> ):void {
			var i:int, theMesh:GPUMesh;
			for (i = pMeshes.length; --i >= 0; ) {
				theMesh = pMeshes[i];
				theMesh._dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
			}
		}
		
		[Inline]
		public static function invalidateColorOffsets( pMeshes:Vector.<GPUMesh> ):void {
			var i:int, theMesh:GPUMesh;
			for (i = pMeshes.length; --i >= 0; ) {
				theMesh = pMeshes[i];
				theMesh._dirtyFlags |= GPUAttribute.TYPE_07_COLOR_OFFSET;
			}
		}
		
		[Inline]
		public static function invalidateColorMultipliers( pMeshes:Vector.<GPUMesh> ):void {
			var i:int, theMesh:GPUMesh;
			for (i = pMeshes.length; --i >= 0; ) {
				theMesh = pMeshes[i];
				theMesh._dirtyFlags |= GPUAttribute.TYPE_06_COLOR_MULTIPLIER;
			}
		}
		
		[Inline]
		public static function setMeshesPivot( pMeshes:Vector.<GPUMesh>, pPivotX:Number, pPivotY:Number ):void {
			var i:int, theMesh:GPUMesh;
			for (i = pMeshes.length; --i >= 0; ) {
				theMesh = pMeshes[i];
				theMesh._pivotX = pPivotX;
				theMesh._pivotY = pPivotY;
				theMesh._dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
			}
		}
		
		public function setVertices( pVertices:Vector.<Number> ):void {
			var theVerts:Vector.<Number> = _vertexPositions._points;
			for (var i:int = 0, iLen:int = theVerts.length; i < iLen; i++) {
				theVerts[i] = pVertices[i];
			}
			_vertexPositions.updateBoundsRadial();
			_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
		}
		
		public function setColor(pColor:uint, pAsOffset:Boolean):void {
			if (pAsOffset) {
				if (!_colorOffset) _colorOffset = new GPUColor();
				_colorOffset.setHex( pColor );
				_dirtyFlags |= GPUAttribute.TYPE_07_COLOR_OFFSET;
			} else {
				if (!_colorMultiplier) _colorMultiplier = new GPUColor();
				_colorMultiplier.setHex( pColor );
				_dirtyFlags |= GPUAttribute.TYPE_06_COLOR_MULTIPLIER;
				
				//Should check if transparent? And cull it out? Maybe...
			}
		}
		
		public function setPivot(pPivotX:Number, pPivotY:Number):void {
			_pivotX = pPivotX;
			_pivotY = pPivotY;
			_dirtyFlags |= GPUAttribute.TYPE_04_POSITION_XY;
		}
		
		//Parent is only a GETTER to the public classes:
		public function get parent():GPUContainer { return _parent; }
		
		public function get constant00():GPUConstant { return _constant00; }
		public function set constant00(value:GPUConstant):void {
			_constant00 = value;
			
			_dirtyFlags |= GPUAttribute.TYPE_00_CONSTANT;
		}
		
		public function get constant01():GPUConstant { return _constant01; }
		public function set constant01(value:GPUConstant):void {
			_constant01 = value;
			
			_dirtyFlags |= GPUAttribute.TYPE_01_CONSTANT;
		}
		
		public function get constant02():GPUConstant { return _constant02; }
		public function set constant02(value:GPUConstant):void {
			_constant02 = value;
			
			_dirtyFlags |= GPUAttribute.TYPE_02_CONSTANT;
		}
		
		public function get constant03():GPUConstant { return _constant03; }
		public function set constant03(value:GPUConstant):void {
			_constant03 = value;
			
			_dirtyFlags |= GPUAttribute.TYPE_03_CONSTANT;
		}
		
		//public function get indexingPattern():Vector.<uint> { return _indexingPattern; }
	}
}