package com.bigp.stage3d {
	import flash.display3D.Context3D;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUContainer extends GPU_IdObject {
		
		internal var _parent:GPUContainer;
		internal var _children:Vector.<GPUContainer>;
		public var _linkNext:GPUContainer;
		
		internal var _filter:GPUFilter;
		
		public var gpuState:GPUState;
		
		public function GPUContainer() {
			_children = new Vector.<GPUContainer>();
		}
		
		public function dispose():void {
			if (!_children) return;
			
			if (_parent) removeSelf();
			_children.length>0 && removeChildren(true);
			_children = null;
			_linkNext = null;
		}
		
		public function addChild( pChild:GPUContainer ):void {
			_children[_children.length] = pChild;
			pChild._parent = this;
			GPU.root._isHierarchyDirty = true;
		}
		
		public function addChildAt( pChild:GPUContainer, pID:int ):void {
			if (pID >= _children.length || pID < 0) throw new Error("Out of meshes bounds.");
			
			pChild._parent = this;
			_children.splice(pID, 0, pChild);
			GPU.root._isHierarchyDirty = true;
		}
		
		public function removeChild( pChild:GPUContainer ):Boolean {
			if (pChild._parent != this) return false;
			
			var id:int = _children.indexOf( pChild );
			_children.splice(id, 1);
			pChild._parent = null;
			pChild._linkNext = null;
			
			GPU.root._isHierarchyDirty = true;
			return true;
		}
		
		public function removeSelf():Boolean {
			if (!_parent) return false;
			_parent.removeChild(this);
			return true;
		}
		
		public function removeChildren( pAndDispose:Boolean = false ):void {
			var i:int = _children.length;
			if(pAndDispose) {
				for (; --i >= 0; ) {
					_children[i].dispose();
					_children[i]._parent = null;
					_children[i]._linkNext = null;
				}
			} else {
				for (; --i >= 0; ) {
					_children[i]._parent = null;
					_children[i]._linkNext = null;
				}
			}
			
			_children.length = 0;
			
			GPU.root._isHierarchyDirty = true;
		}
		
		public function removeChildrenFast():void {
			_children.length = 0;
		}
		
		internal function get context():Context3D { return GPU._INSTANCE._context; }
		
		public function get filter():GPUFilter { return _filter; }
		public function set filter(value:GPUFilter):void {
			if (_filter) {
				trace("A filter existed on this container already.");
			}
			
			_filter = value;
		}
	}
}