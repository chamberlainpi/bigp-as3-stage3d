package com.bigp.stage3d {

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUPositionVertices {
		
		private var _boundsRadial:Number = 0;
		internal var _points:Vector.<Number>;
		
		public function GPUPositionVertices( pPoints:Vector.<Number> ) {
			_points = pPoints;
			updateBoundsRadial();
		}
		
		[Inline]
		public final function updateBoundsRadial():void {
			var maxRadius:Number = 0;
			for (var j:int=0, jLen:int=_points.length; j<jLen; j+=2) {
				var theX:Number = _points[j];
				var theY:Number = _points[int(j + 1)];
				var theDist:Number = Math.sqrt(theX * theX + theY * theY);
				if (theDist > maxRadius) maxRadius = theDist;
			}
			
			_boundsRadial = maxRadius;
		}
		
		public function get points():Vector.<Number> { return _points; }
	}
}