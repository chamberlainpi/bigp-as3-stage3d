package com.bigp.stage3d {
	import flash.display3D.Context3D;
	
	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public interface IGPUHandleLostContext {
		function onContextCreated( pContext:Context3D ):void
	}
	
}