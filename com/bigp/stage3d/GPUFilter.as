package com.bigp.stage3d {
	import com.bigp.Lib;
	import com.bigp.utils.MathUtils;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProgramType;
	import flash.display3D.Context3DTextureFormat;
	import flash.display3D.IndexBuffer3D;
	import flash.display3D.textures.Texture;
	import flash.display3D.VertexBuffer3D;
	import flash.geom.Rectangle;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUFilter extends GPUProgram implements IGPUHandleLostContext {
		
		internal var _vertexBuffer:VertexBuffer3D;
		internal var _indexBuffer:IndexBuffer3D;
		internal var _texture:Texture;
		private var _textureWidth:int;
		private var _textureHeight:int;
		
		private var _frameIndices:Vector.<uint> = new <uint>[0, 1, 2, 2, 1, 3];
		private var _frameVertices:Vector.<Number> = new <Number> [
			-1, 1, 0, 0,
			1, 1, 1, 0,
			-1, -1, 0, 1,
			1, -1, 1, 1
		];
		
		private var _constantsVertex:Vector.<Number>;
		private var _constantsFragment:Vector.<Number>;
		
		
		public function GPUFilter() {
			super();
			
			assembleProgram(
				[
					"mov op, va0",
					"add v0, va1, vc0",
					"add v1, va1, vc1",
					"add v2, va1, vc2"
				],
				[
					"tex ft0, v0, fs0 <2d, linear, none, nomip>",
					"tex ft1, v1, fs0 <2d, linear, none, nomip>",
					"tex ft2, v2, fs0 <2d, linear, none, nomip>",
					"mov ft4.x, ft0.x",
					"mov ft4.y, ft1.y",
					"mov ft4.z, ft2.z",
					"max ft4.w, ft0.w, ft1.w",
					"max ft4.w, ft4.w, ft2.w",
					"mov oc, ft4"
					//"mo"
				],
				false
			);
			
			_constantsVertex = new <Number>[
				0.5, 0.5, 0, 0,
				0.2, 0.2, 0, 0,
				0.1, 0.1, 0, 0
			];
			
			_constantsFragment = new <Number>[
				0, 1, 2, 0.5
			];
			
			
			onContextCreated( context );
		}
		
		/* INTERFACE com.bigp.stage3d.IGPUHandleLostContext */
		
		public override function onContextCreated(pContext:Context3D):void {
			var theView:Rectangle = GPU._INSTANCE._config.viewRect;
			
			if (_texture) {
				_texture.dispose();
				_vertexBuffer.dispose();
				_indexBuffer.dispose();
			}
			
			_textureWidth = MathUtils.getNextPowerOf2(theView.width);
			_textureHeight = MathUtils.getNextPowerOf2(theView.height);
			_texture = pContext.createTexture( _textureWidth, _textureHeight, Context3DTextureFormat.BGRA, true );
			_vertexBuffer = pContext.createVertexBuffer(4, 4);
			_indexBuffer = pContext.createIndexBuffer(6);
			
			//Upload to the vertex-buffer and index-buffer:
			_vertexBuffer.uploadFromVector(_frameVertices, 0, 4);
			_indexBuffer.uploadFromVector(_frameIndices, 0, 6);
			
			super.onContextCreated(pContext);
		}
		
		public function preRender():void {
			var theConfig:GPUConfig = GPU._INSTANCE._config;
			context.setRenderToTexture( _texture, theConfig.enableDepthAndStencil, theConfig.antiAlias );
			context.clear(0, 0, 0, 0, GPU._INSTANCE.clearDepth, GPU._INSTANCE.clearStencil, GPU._INSTANCE.clearMask );
		}
		
		public function updateConstants():void {
			//TEST!!!
			_constantsVertex[0] = (Lib.STAGE.mouseX-Lib.WIDTH_HALF) / _textureWidth;
			_constantsVertex[1] = (Lib.STAGE.mouseY-Lib.HEIGHT_HALF) / _textureHeight;
			_constantsVertex[4] = _constantsVertex[0] * .5;
			_constantsVertex[5] = _constantsVertex[1] * .5;
			_constantsVertex[8] = _constantsVertex[0] * .25;
			_constantsVertex[9] = _constantsVertex[1] * .25;
			
			if(_constantsVertex && _constantsVertex.length > 0) {
				context.setProgramConstantsFromVector( Context3DProgramType.VERTEX, 0, _constantsVertex );
				GPU._INSTANCE._numOfUploads++;
				GPU._INSTANCE._bytesUploaded += _constantsVertex.length * 4;
			}
			if (_constantsFragment && _constantsFragment.length > 0) {
				context.setProgramConstantsFromVector( Context3DProgramType.FRAGMENT, 0, _constantsFragment );
				GPU._INSTANCE._numOfUploads++;
				GPU._INSTANCE._bytesUploaded += _constantsFragment.length * 4;
			}
		}
		
		public function renderTexture():void {
			context.setRenderToBackBuffer();
			context.setTextureAt( 0, _texture );
			context.setVertexBufferAt( 0, _vertexBuffer, 0, "float2" );
			context.setVertexBufferAt( 1, _vertexBuffer, 2, "float2" );
			context.setVertexBufferAt( 2, null );
			context.setVertexBufferAt( 3, null );
			context.setProgram(_program);
			updateConstants();
			context.drawTriangles( _indexBuffer, 0, 2 );
			GPU.renderer._numOfDrawCalls++;
		}
		
		public function postRender():void {
			context.setTextureAt( 0, null );
			context.setVertexBufferAt( 0, null );
			context.setVertexBufferAt( 1, null );
		}
	}
}