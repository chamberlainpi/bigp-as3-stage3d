package com.bigp.stage3d {
	import flash.display.BitmapData;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DTextureFormat;
	import flash.display3D.textures.Texture;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.utils.Endian;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUTexture extends GPU_Disposable implements IGPUHandleLostContext {
		private static var _HELPER_BITMAP_RECT:Rectangle = new Rectangle();
		private static var _TEXTURE_ID:int = 0;
		
		public var texture:Texture;
		public var bitmapBytes:ByteArray;
		public var width:int;
		public var height:int;
		public var widthPow2:int;
		public var heightPow2:int;
		
		private var _name:String;
		private var _textureId:int;
		
		public function GPUTexture(pName:String) {
			super(false);
			
			_textureId = ++_TEXTURE_ID;
			_name = pName || ("texture_" + _textureId);
			
			GPU._INSTANCE.whenContextLossHandled.add( onContextCreated );
		}
		
		
		public function init(pBitmap:BitmapData):void {
			//Set the helperRect to the bitmap's size (remain at X,Y 0,0 though)
			_HELPER_BITMAP_RECT.width = width = pBitmap.width;
			_HELPER_BITMAP_RECT.height = height = pBitmap.height;
			
			//Important to construct the bytearray in advance to set the Endian type correctly before copying the bitmap.
			bitmapBytes = new ByteArray();
			bitmapBytes.endian = Endian.LITTLE_ENDIAN;
			pBitmap['copyPixelsToByteArray'](_HELPER_BITMAP_RECT, bitmapBytes);
			
			//Remove the bitmap since it's no longer needed:
			pBitmap.dispose();
			
			onContextCreated( context );
		}
		
		public override function dispose():void {
			if (!texture) return;
			
			texture.dispose();
			bitmapBytes.clear();
			
			texture = null;
			bitmapBytes = null;
			
			GPU._INSTANCE.whenContextLossHandled.remove( onContextCreated );
			super.dispose();
		}
		
		public function uploadBitmapData( pBitmapData:BitmapData, pRect:Rectangle=null ):void {
			if (!bitmapBytes) {
				bitmapBytes = new ByteArray();
				bitmapBytes.endian = Endian.LITTLE_ENDIAN;
			}
			
			pBitmapData['copyPixelsToByteArray']( pRect || pBitmapData.rect, bitmapBytes );
			
			if (CONFIG::GPU_OFF) { return; }
			if (!texture) return;
			
			GPU._INSTANCE._numOfUploads++;
			GPU._INSTANCE._bytesUploaded += bitmapBytes.length;
			texture.uploadFromByteArray( bitmapBytes, 0, 0 );
		}
		
		/* INTERFACE com.bigp.stage3d.IGPUHandleLostContext */
		
		public function onContextCreated(pContext:Context3D):void {
			if (CONFIG::GPU_OFF) { return; }
			GPU._INSTANCE._numOfUploads++;
			GPU._INSTANCE._bytesUploaded += bitmapBytes.length;
			texture = pContext.createTexture( width, height, Context3DTextureFormat.BGRA, false);
			texture.uploadFromByteArray( bitmapBytes, 0, 0 );
		}
	}
}