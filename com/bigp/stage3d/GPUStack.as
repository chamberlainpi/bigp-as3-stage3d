package com.bigp.stage3d {
	import com.bigp.Lib;
	import flash.display3D.Context3D;
	import flash.display3D.IndexBuffer3D;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.Endian;
	import avm2.intrinsics.memory.*;
	
	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUStack extends GPU_Disposable implements IGPUHandleLostContext {
		internal var _attributes:Vector.<GPUAttribute>;
		internal var _attributesByType:Dictionary;
		internal var _numVertices:int = -1;
		internal var _numIndices:int = 0;
		internal var _numTriangles:int = 0;
		internal var _lengthOfFormat:int = 0;
		internal var _index:IndexBuffer3D;
		internal var _bytesShared:ByteArray;
		internal var _isDirtySize:Boolean = true;
		internal var _isDirtyAttributes:Boolean = true;
		private var _autoType:int = 1;
		private var _totalBytes:int;
		
		public var indexSizeMultiplier:Number = 3;
		public var nextAvailableVertexID:int = 0;
		public var vertexChangedMax:int = 0;
		public var vertexChangedMin:int = 0;
		
		public function GPUStack( pNumVertices:int ) {
			super(true);
			
			_attributes = new Vector.<GPUAttribute>();
			_attributesByType = new Dictionary();
			numVertices = pNumVertices;
			
			GPU._INSTANCE.whenContextLossHandled.add( onContextCreated );
		}
		
		public override function dispose():void {
			if (!_index) return;
			_index.dispose();
			CONFIG::domainmem {
				Lib.DOMAIN_MEM = null;
			}
			_bytesShared.clear();
			_bytesShared = null;
			
			GPU._INSTANCE.whenContextLossHandled.remove( onContextCreated );
			
			for (var i:int=_attributes.length; --i>=0;) {
				var theAttrib:GPUAttribute = _attributes[i];
				theAttrib.dispose();
			}
			_attributes.length = 0;
			
			_index = null;
			_attributes = null;
			_attributesByType = null;
			
			super.dispose();
		}
		
		public function addAttribute( pFields:int, pType:int ):GPUAttribute {
			CONFIG::debug {
				if (pFields < 1 || pFields > 4) {
					throw new Error("A buffer must have between 1 and 4 fields (to fill one vertex-attribute register): " + pFields);
				}
			}
			
			GPU._CAN_INSTANTIATE = true;
			var theAttribute:GPUAttribute = new GPUAttribute(pFields);
			theAttribute._stack = this;
			theAttribute._type = pType;
			_lengthOfFormat += pFields;
			_attributes[_attributes.length] = theAttribute;
			
			if (_attributesByType[pType]) {
				throw new Error("There is already an attribute allocated in the stack with the type: " + pType );
			}
			
			_attributesByType[pType] = theAttribute;
			
			return theAttribute;
		}
		
		/* INTERFACE com.bigp.stage3d.IGPUHandleLostContext */
		
		public function onContextCreated(pContext:Context3D):void {
			if (_index) {
				_index.dispose();
				_index = null;
			}
			
			_index = pContext.createIndexBuffer( _numIndices );
			
			//Pre-add the total-bytes by the number of indices bytes.
			_totalBytes = _numIndices << 1;
			
			var i:int = 0,
				iLen:int = _attributes.length,
				theBuffer:GPUAttribute;
			
			for (i=0; i<iLen; i++) {
				theBuffer = _attributes[i];
				theBuffer._bytesSharedStart = _totalBytes;
				theBuffer._bytesRequired = (_numVertices * theBuffer._numOfFields) << 2;
				_totalBytes += theBuffer._bytesRequired;
			}
			
			if (!_bytesShared) {
				_bytesShared = new ByteArray();
				_bytesShared.endian = Endian.LITTLE_ENDIAN;
				_bytesShared.length = theBuffer._bytesSharedStart + theBuffer._bytesRequired;
				
				CONFIG::domainmem {
					//Immediately set as current Domain Memory bytes:
					Lib.APP_DOMAIN.domainMemory = _bytesShared;
				}
				
				//Write default values for individual attributes:
				for (i=0; i<iLen; i++) {
					theBuffer = _attributes[i];
					var thePos:uint = theBuffer._bytesSharedStart;
					var theDefault:Number = theBuffer.defaultValue;
					for (var n:int = _numVertices * theBuffer._numOfFields; --n >= 0; ) {
						CONFIG::domainmem {
							sf32(theDefault, thePos);
						}
						CONFIG::regularmem {
							_bytesShared.position = thePos;
							_bytesShared.writeFloat( theDefault );
						}
						thePos += 4;
					}
				}
			}
			
			if (CONFIG::GPU_OFF) { return; }
			
			//Upload a blank index-buffer first:
			GPU._INSTANCE._numOfUploads++;
			GPU._INSTANCE._bytesUploaded += _numIndices << 1;
			_index.uploadFromByteArray( _bytesShared, 0, 0, _numIndices );
			
			//Upload vertex-buffers' default values:
			for (i=0; i<iLen; i++) {
				theBuffer = _attributes[i];
				theBuffer.onContextCreated(pContext);
			}
		}
		
		[Inline]
		public final function getAttributeByType( pType:int ):GPUAttribute {
			return _attributesByType[pType];
		}
		
		public function get numVertices():int { return _numVertices; }
		public function set numVertices(value:int):void {
			_numVertices = value;
			_numIndices = value * indexSizeMultiplier;
			_isDirtySize = true;
		}
		
		public override function toString():String {
			return "[GPUStack #" + _id +" allocated: " + _numTriangles + " tris, " + _numIndices + " indices, " + _numVertices + " verts.]";
		}
		
		[Inline]
		public function get attributes():Vector.<GPUAttribute> { return _attributes; }
		public function get totalBytes():int { return _totalBytes; }
	}
}