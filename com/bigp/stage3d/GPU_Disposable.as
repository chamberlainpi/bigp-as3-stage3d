package com.bigp.stage3d {
	import flash.display3D.Context3D;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPU_Disposable extends GPU_IdObject {
		public var isDisposed:Boolean = false;
		
		public function GPU_Disposable(pForcedInstantiation:Boolean) {
			super();
			CONFIG::debug {
				if (!pForcedInstantiation && !GPU._CAN_INSTANTIATE) throw new Error("Cannot instantiate object directly, see GPU class.");
				GPU._CAN_INSTANTIATE = false;
			}
		}
		
		public function dispose():void {
			isDisposed = true;
		}
		
		internal function get context():Context3D { return GPU._INSTANCE._context; }
	}
}