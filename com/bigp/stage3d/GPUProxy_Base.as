package com.bigp.stage3d {
	import flash.display3D.Context3D;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUProxy_Base extends GPU_Disposable {
		
		protected var _gpu:GPU;
		
		public function GPUProxy_Base() {
			super(true);
			
			_gpu = GPU._INSTANCE;
		}
		
		internal function get config():GPUConfig { return _gpu._config; }
		internal function get inventory():GPUProxyInventory { return _gpu._proxyInventory; }
	}
}