package com.bigp.stage3d {
	import flash.geom.Rectangle;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUConfig {
		
		public var mode:String = "auto";
		public var profile:String = "baseline";
		
		public var wantsBestResolution:Boolean = false;
		public var enableDepthAndStencil:Boolean = false;
		public var antiAlias:int = 0;
		public var viewRect:Rectangle;
		public var viewRectOrigin:Rectangle;
		internal var _viewRectInverse:Rectangle;
		public var isStageRect:Boolean = false;
		
		public function GPUConfig() {
			_viewRectInverse = new Rectangle();
		}
		
		public function init():void {
			if (viewRect) {
				viewRectOrigin = new Rectangle(0, 0, viewRect.width, viewRect.height);
			}
		}
		
		[Inline]
		internal final function inline_updateInverse():void {
			_viewRectInverse.width = 2 / viewRect.width;
			_viewRectInverse.height = -2 / viewRect.height;
		}
		
		public function get viewRectInverse():Rectangle { return _viewRectInverse; }
	}
}