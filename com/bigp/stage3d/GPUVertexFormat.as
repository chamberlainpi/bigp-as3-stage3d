package com.bigp.stage3d {
	import flash.utils.ByteArray;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUVertexFormat {
		public var numFields:int = 1;
		public var attributeID:int = 0;
		public var name:String;
		public var isConstantIndex:Boolean = false;
		public var constantIndex:int = 0;
		public var defaultValues:Vector.<Number>;
		private var _helperValues:Vector.<Number>;
		
		public function GPUVertexFormat(pName:String, pNumFields:int, pAttributeIndex:int) {
			name = pName;
			numFields = pNumFields;
			attributeID = pAttributeIndex;
		}
		
		public function setAsConstant( pFixedNumber:int=0 ):GPUVertexFormat {
			isConstantIndex = true;
			constantIndex = pFixedNumber;
			return this;
		}
		
		public function setDefaultValues(pVertex0:Array, pVertex1:Array, pVertex2:Array, pVertex3:Array ):void {
			defaultValues = new Vector.<Number>();
			
			var verticesValues:Array = [pVertex0, pVertex1, pVertex2, pVertex3];
			for (var i:int=0, iLen:int=verticesValues.length; i<iLen; i++) {
				var theValues:Array = verticesValues[i];
				for (var j:int=0, jLen:int=theValues.length; j<jLen; j++) {
					var theValue:Number = Number(theValues[j]);
					defaultValues[defaultValues.length] = theValue;
				}
			}
		}
		
		public function getDefaultValues( pVertexID:int ):Vector.<Number> {
			if (!_helperValues) { _helperValues = new Vector.<Number>(numFields, true); }
			var valueID:int = 0;
			for (var n:int = pVertexID * numFields, nLen:int = n + numFields; n < nLen; n++) {
				_helperValues[valueID++] = defaultValues[n];
			}
			return _helperValues;
		}
		
		public function appendDefaultValuesTo( pList:Vector.<Number>, pVertexID:int, pStartAt:int ):void {
			var vertexValues:Vector.<Number>;
			vertexValues = getDefaultValues( pVertexID );
			var i:int = 0, iLen:int = vertexValues.length;
			
			if(pStartAt<0) {
				for (; i<iLen; i++) {
					pList[pList.length] = vertexValues[i];
				}
			} else {
				for (; i<iLen; i++) {
					pList[pStartAt++] = vertexValues[i];
				}
			}
		}
		
		public function appendDefaultValuesToBytes( pBytes:ByteArray, pVertexID:int, pStartAt:int):void {
			var vertexValues:Vector.<Number>;
			vertexValues = getDefaultValues( pVertexID );
			var i:int = 0, iLen:int = vertexValues.length;
			
			if(pStartAt<0) {
				for (; i<iLen; i++) {
					pBytes.writeFloat( vertexValues[i] );
				}
			} else {
				pBytes.position = pStartAt * 4;
				for (; i<iLen; i++) {
					pBytes.writeFloat( vertexValues[i] );
				}
			}
		}
	}
}