package com.bigp.stage3d {
	import com.bigp.Lib;
	import flash.display3D.Context3D;
	import flash.utils.ByteArray;
	import avm2.intrinsics.memory.*;
	
	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class GPUProxyRenderer extends GPUProxy_Base {
		
		private static var _FLOAT_STRINGS:Vector.<String> = new < String > ["float0", "float1", "float2", "float3", "float4"];
		internal var _indexAddress:uint;
		internal var _numOfDrawCalls:int = 0;
		internal var _numOfPolys:int = 0;
		internal var _helperFloats:Vector.<Number>;
		internal var _currentStack:GPUStack;
		
		public function GPUProxyRenderer() {
			super();
			
			_helperFloats = new Vector.<Number>(4, true);
		}
		
		public function render():void {
			_numOfDrawCalls = 0;
			_numOfPolys = 0;
			
			if (_gpu.root._isHierarchyDirty) {
				_gpu.root._isHierarchyDirty = false;
				createContainerLinkList( _gpu.root );
			}
			
			iterateStatesWithMeshes( _gpu.root );
		}
		
		private function createContainerLinkList( pParent:GPUContainer ):GPUContainer {
			var current:GPUContainer = pParent;
			var theChildren:Vector.<GPUContainer> = current._children;
			var theMesh:GPUMesh;
			
			for (var i:int=0, iLen:int=theChildren.length; i<iLen; i++) {
				var theChild:GPUContainer = theChildren[i];
				if (theChild is GPUMesh) {
					theMesh = GPUMesh(theChild);
					if (!theMesh._visible) {
						continue;
					}
				}
				
				current._linkNext = theChild;
				if (theChild._children.length > 0) {
					current = createContainerLinkList( theChild );
				} else {
					current = theChild;
				}
			}
			
			return current;
		}
		
		private function iterateStatesWithMeshes( pRoot:GPUContainer ):void {
			_currentStack = null;
			
			var currentState:GPUState = pRoot.gpuState || GPU.state;
			currentState.iterateMeshes( this, pRoot );
			
			_currentStack = null;
		}
		
		[Inline]
		public final function appendMeshToIndexBytes(pMesh:GPUMesh):void {
			//Add the number of triangle to render in this mesh:
			var theMeshIndices:Vector.<int> = pMesh._indexingPattern;
			var startIndex:int = pMesh._stackStartIndex;
			var theBytes:ByteArray = _currentStack._bytesShared;
			_currentStack._numTriangles += int(pMesh._indexingPattern.length / 3);
			
			CONFIG::regularmem {
				theBytes.position = _indexAddress;
			}
			
			//Append the triangle patterns to the index-bytes:
			for (var k:int = 0, kLen:int = theMeshIndices.length; k < kLen; k++) {
				CONFIG::domainmem {
					si16( startIndex + theMeshIndices[k], _indexAddress );
					_indexAddress += 2;
				}
				CONFIG::regularmem {
					theBytes.writeShort( startIndex + theMeshIndices[k] );
				}
			}
			
			CONFIG::regularmem {
				_indexAddress += 2 * theMeshIndices.length;
			}
		}
		
		[Inline]
		public final function drawCurrentStack():void {
			var theContext:Context3D = context;
			var theAttribs:Vector.<GPUAttribute> = _currentStack._attributes;
			var theAttrib:GPUAttribute;
			var i:int = 0, iLen:int = theAttribs.length;
			
			if (_currentStack._isDirtyAttributes) {
				var theMin:int = _currentStack.vertexChangedMin;
				var theMax:int = _currentStack.vertexChangedMax;
				
				if (theMax!==-1) {
					_currentStack._isDirtyAttributes = false;
					for (i=0; i<iLen; i++) {
						theAttrib = theAttribs[i];
						if (!theAttrib._isDirty || !theAttrib.isActive) continue;
						theAttrib._isDirty = false;
						theAttrib.uploadRange( _currentStack.vertexChangedMin, _currentStack.vertexChangedMax );
					}
				}
			}
			
			//Set the vertex-attributes:
			for (i = 0; i < iLen; i++) {
				theAttrib = theAttribs[i];
				if (!theAttrib.isActive) continue;
				theContext.setVertexBufferAt( i, theAttrib._vertex, 0, _FLOAT_STRINGS[ theAttrib._numOfFields ] );
			}
			
			//Upload the indices:
			_currentStack._index.uploadFromByteArray( _currentStack._bytesShared, 0, 0, _currentStack._numTriangles * 3);
			
			theContext.drawTriangles( _currentStack._index, 0, _currentStack._numTriangles );
			_numOfPolys += _currentStack._numTriangles;
			_numOfDrawCalls++;
		}
		
		public function get numOfDrawCalls():int { return _numOfDrawCalls; }
		public function get numOfPolys():int { return _numOfPolys; }
	}
}