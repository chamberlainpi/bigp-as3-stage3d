package tests {
	import com.bigp.doublee.EE;
	import com.bigp.doublee.EEWorld;
	import com.bigp.Lib;
	import com.bigp.stage3d.GPU;
	import com.bigp.utils.FullscreenUtils;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	
	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class Test_GPU {
		public static var LABEL:TextField;
		
		private var message:Array;
		
		protected var _gpu:GPU;
		public var stage:Stage;
		public var moreMessage:String;
		
		public function Test_GPU() {
			stage = Lib.STAGE;
			message = [];
		}
		
		public final function init():Test_GPU {
			Lib.initInfo();
			
			_gpu = GPU.getInstance();
			_gpu.whenInitialized.add( onGPUReady );
			_gpu.init().backgroundColor.r = 0.5;
			
			EE.init();
			EE.world = new EEWorld();
			EE.signals.whenUpdateFrame.add( onUpdateInternally );
			
			
			var larger:Sprite = new Sprite();
			larger.scaleX = larger.scaleY = 2;
			stage.addChild(larger);
			
			LABEL = new TextField();
			LABEL.defaultTextFormat = new TextFormat("Arial", 8, 0xffffffff);
			LABEL.autoSize = TextFieldAutoSize.LEFT;
			LABEL.condenseWhite = true;
			LABEL.x = 5;
			LABEL.y = 5;
			LABEL.text = "____";
			
			larger.addChild(LABEL);
			
			stage.addEventListener(MouseEvent.CLICK, onClick);
			
			return this;
		}
		
		//OVERRIDE
		protected function onGPUReady():void {
			Lib.KEYS.bind(Keyboard.F, FullscreenUtils.toggleCallback);
			Lib.KEYS.bind(Keyboard.ESCAPE, GPU.dispose, [true]);
		}
		
		protected function onGPUUpdate():void {}
		protected function onClick(e:MouseEvent):void {
			GPU.context['dispose'](true);
		}
		
		private function onUpdateInternally():void {
			if (!_gpu || !_gpu.state) return;
			
			onGPUUpdate();
			printLabel( moreMessage );
		}
		
		protected function printLabel( pMore:String=null ):void {
			var draws:int = GPU.renderer.numOfDrawCalls;
			var memoryType:String;
			CONFIG::domainmem { memoryType = "Domain"; }
			CONFIG::regularmem { memoryType = "ByteArray"; }
			
			message[0] = "FPS: " + EE.time.fps
			message[1] = "MEM.TYPE: " + memoryType
			message[2] = "DRAWS: " + draws;
			message[3] = "TRIS: " + GPU.renderer.numOfPolys,
			message[4] = "UPLOADS: " + GPU.numOfUploads,
			message[5] = "BYTES-UPLOADED: " + GPU.bytesUploaded,
			//message[4] = "ERROR-CHECKING: " + GPU.context.enableErrorChecking,
			message[6] = moreMessage;	
			
			LABEL.text = message.join("\n");
		}
		
	}
}