package tests {
	import com.bigp.doublee.EE;
	import com.bigp.Lib;
	import com.bigp.stage3d.GPU;
	import com.bigp.stage3d.GPUAttribute;
	import com.bigp.stage3d.GPUConstant;
	import com.bigp.stage3d.GPUMesh;
	import com.bigp.stage3d.GPUPositionVertices;
	import com.bigp.stage3d.GPUStack;
	import com.bigp.stage3d.GPUState;
	import com.bigp.stage3d.GPUStateSimple;
	import com.bigp.utils.ColorUtils;
	import com.bigp.utils.FullscreenUtils;
	import com.bigp.utils.WatchUtils;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class TestSpiralRainbow extends Test_GPU {
		private static var _VALUES_SQRT:Vector.<Number>;
		private static var _VALUES_ASIN:Vector.<Number>;
		private static var _QUADS_POOL:Vector.<GPUMesh>;
		private static var _MAX_QUADS:int = 16128;
		
		private var theQuad:GPUMesh;
		private var theQuads:Vector.<GPUMesh>;
		private var timer:Number = 0;
		private var mult:int;
		public var totalQuads:int = 0;
		private var lastX:Number = 0;
		private var theStack:GPUStack;
		private var indicesOfQuad:Vector.<int>;
		private var thePixelSize:int;
		private var quadsPerMultiples:int;
		private var quadVertices:GPUPositionVertices;
		private var attrConstants:GPUAttribute;
		private var quadConstants:Vector.<GPUConstant>;
		private var firstCycle:Boolean = true;
		
		protected override function onGPUReady():void {
			super.onGPUReady();
			
			mult = 1;
			thePixelSize = 32;
			quadsPerMultiples = 128;
			theQuads = new Vector.<GPUMesh>();
			indicesOfQuad = new <int>[0, 1, 2,   2, 1, 3];
			quadVertices = new GPUPositionVertices(new Vector.<Number>(8, true));
			
			poolValues();
			onResizeStack( 0 );
			
			Lib.KEYS.bind(Keyboard.D, onResizeStack, [ 1 ]);
			Lib.KEYS.bind(Keyboard.A, onResizeStack, [ -1 ]);
			Lib.KEYS.bind(Keyboard.W, onResizePixels, [ 1 ]);
			Lib.KEYS.bind(Keyboard.S, onResizePixels, [ -1]);
			
			CONFIG::debug {
				WatchUtils.init(stage, 120, 5);
				WatchUtils.INST.addProperty2(this, "totalQuads", 0x002288, 10000, "Total Quads ##");
			}
		}
		
		private function poolValues():void {
			//Prepare square-roots AND ASIN values in a pool:
			_VALUES_SQRT = new Vector.<Number>(_MAX_QUADS, true);
			_VALUES_ASIN = new Vector.<Number>(_MAX_QUADS, true);
			_QUADS_POOL = new Vector.<GPUMesh>();
			for (var i:int=_VALUES_SQRT.length; --i>=0;) {
				_VALUES_SQRT[i] = Math.sqrt(i + 1);
				_VALUES_ASIN[i] = Math.asin(1 / _VALUES_SQRT[i]);
				
				//Create pool of QUADS too:
				_QUADS_POOL[_QUADS_POOL.length] = new GPUMesh();
			}
			
			quadConstants = new Vector.<GPUConstant>(120, true);
			for (var j:int=0, jLen:int=quadConstants.length; j<jLen; j++) {
				quadConstants[j] = GPU.state.generateConstant();
			}
		}
		
		private function onResizeStack( pMoreOrLess:int ):void {
			if (pMoreOrLess > 0 && mult < 100) {
				if (mult == 127) mult = 0;
				mult++;
			} else if (pMoreOrLess < 0 && mult > 1) {
				mult--;
			} else if (pMoreOrLess != 0) {
				return;
			}
			
			if (theStack) {
				theStack.dispose();
				theStack = null;
				
				for (var j:int=theQuads.length; --j>=0;) {
					var theQuad:GPUMesh = theQuads[j];
					theQuad._linkNext = null; //Nullify quad linkage (very important)
					_QUADS_POOL[_QUADS_POOL.length] = theQuad;
				}
				
				theQuads.length = 0;
				_gpu.root.removeChildrenFast();
			}
			
			theStack = new GPUStack( (quadsPerMultiples << 2) * mult ); //_gpu.add.stack( (quadsPerMultiples << 2) * mult );
			theStack.addAttribute( 2, GPUAttribute.TYPE_04_POSITION_XY );
			theStack.addAttribute( 4, GPUAttribute.TYPE_06_COLOR_MULTIPLIER ).setActiveAndDefault( true, 1 );
			theStack.addAttribute( 4, GPUAttribute.TYPE_00_CONSTANT ).setActiveAndDefault( true, 1 );
			theStack.onContextCreated( GPU.context );
			
			firstCycle = true;
			
			onResizePixels(0, true);
		}
		
		private function onResizePixels( pSizeIncrement:int, pInstantiate:Boolean=false ):void {
			if (!(thePixelSize == 1 && thePixelSize < 0)) {
				thePixelSize += pSizeIncrement;
			}
			
			//Set the template vertices for all quads:
			quadVertices.points[2] = thePixelSize;
			quadVertices.points[5] = thePixelSize;
			quadVertices.points[6] = thePixelSize;
			quadVertices.points[7] = thePixelSize;
			quadVertices.updateBoundsRadial();
			
			totalQuads = quadsPerMultiples * mult;
			
			var i:int = 0; i < totalQuads;
			if (pInstantiate) {
				var lastID:int = -1;
				var currentID:int;
				var constantDivider:Number = totalQuads / quadConstants.length;
				var theConst:GPUConstant;
				
				for (i=0; i < totalQuads; i++) {
					theQuad = _QUADS_POOL[_QUADS_POOL.length-i-1];
					theQuad.buildMesh(4, indicesOfQuad, theStack, quadVertices);
					currentID = int(i / constantDivider);
					if (currentID != lastID) {
						theConst = quadConstants[currentID];
						lastID = currentID;
					}
					
					theQuad.constant00 = theConst;
					_gpu.root.addChild( theQuad );
					theQuads[theQuads.length] = theQuad;
				}
				
				_QUADS_POOL.length -= totalQuads;
			}
			
			var thePivot:Number = thePixelSize * .5;
			GPUMesh.setMeshesPivot( theQuads, thePivot, thePivot );
		}
		
		protected override function onClick(e:MouseEvent):void {
			if(false) {
				for (var i:int = 0, iLen:int = theQuads.length; i < iLen; i += 2) {
					var theQuad:GPUMesh = theQuads[i];
					theQuad.visible = !theQuad.visible;
				}
			} else {
				//super.onClick(e);
				onResizeStack( stage.mouseY<Lib.HEIGHT_HALF ? 1 : -1 ); // */
			}
		}
		
		protected override function onGPUUpdate():void {
			super.onGPUUpdate();
			
			var theX:Number = stage.mouseX - Lib.WIDTH_HALF;
			var theY:Number = stage.mouseY - Lib.HEIGHT_HALF;
			var interpolateX:Number = lastX + (theX - lastX) * 0.05;
			timer += theY * 0.001;
			
			_gpu.backgroundColor.setHex(ColorUtils.hsvToRGB(timer*0.1, 0.3, 0.15));
			
			var theAngle:Number = timer;
			var hypo:Number = interpolateX * 50 * mult;
			var circleSize:Number = hypo / Lib.WIDTH_HALF * (Lib.WIDTH_HALF / (theQuads.length - 1));
			var revolutions:Number = 360 / Math.PI * 2 * mult;
			var segments:Number = (theQuads.length - 1) / revolutions;
			var rotationSpeed:Number = theY * 0.001;
			var colorSpeed:Number = (timer < 0 ? -timer : timer) * 0.1;
			
			if(true) {
				for (var i:int=0, iLen:int=theQuads.length; i<iLen; i++) {
					theQuad = theQuads[i];
					var spiralSqrt:Number = _VALUES_SQRT[i];
					var pointDistance:Number = spiralSqrt * circleSize;
					theAngle += _VALUES_ASIN[i] * segments;
					
					theQuad.setPositionAndRotation(
						Math.cos(theAngle) * pointDistance,
						Math.sin(theAngle) * pointDistance,
						theQuad.rotation + rotationSpeed
					);
					
					var theColor:uint = 0xff000000 | ColorUtils.hsvToRGB(spiralSqrt * 0.01 + colorSpeed, 1, 1);
					theQuad.setColor( theColor, false );
				}
				firstCycle = false;
			}
			
			if(true) {
				for (var j:int=0, jLen:int=quadConstants.length; j<jLen; j++) {
					var theConst:GPUConstant = quadConstants[j];
					theConst.x = Math.cos(theAngle * 0.8) * circleSize;
					theConst.y = Math.sin(theAngle * 0.8) * circleSize;
					theAngle += 10;
				}
			}
			
			moreMessage = "QUADS: " + theQuads.length;
			lastX = interpolateX;
		}
	}
}