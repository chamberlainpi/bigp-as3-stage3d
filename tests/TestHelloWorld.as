package tests {
	import asset.Assets;
	import com.bigp.doublee.EE;
	import com.bigp.Lib;
	import com.bigp.stage3d.GPU;
	import com.bigp.stage3d.GPUBuffer;
	import com.bigp.stage3d.GPUMesh;
	import com.bigp.stage3d.GPUSprite;
	import com.bigp.stage3d.GPUStack;
	import com.bigp.stage3d.GPUTexture;
	import com.bigp.utils.ColorUtils;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class TestHelloWorld extends Test_GPU {
		
		private var theBuffer:GPUStack;
		private var theSprite:GPUMesh;
		protected var timeCounter:Number = 0;
		
		protected override function onGPUReady():void {
			var theTexture:GPUTexture = GPU.add.textureFromAsset(Assets.TEST_IMAGE);
			theBuffer = GPU.add.buffer(31, null, _gpu.state); //Max buffers is 31 apparently... hmmm
			theSprite = theBuffer.makeSprite();
			theBuffer.makeSprite();
			
			_gpu.state.textures = [theTexture];
			
			//theBuffer
		}
		
		protected override function onGPUUpdate():void {
			super.onGPUUpdate();
			
			var movement:Number = (stage.mouseX - Lib.WIDTH_HALF) / Lib.WIDTH_HALF;
			timeCounter += EE.time.delta * Math.max(0.02, Math.abs(movement) * 4) * (movement<0 ? -1 : 1);
			var theTime:Number = timeCounter * 0.001;
			var total:int = theBuffer.numOfQuads * 4 + 1;
			for (var g:int = 4; g < total; g += 4) {
				var angle:Number = theTime + g * 0.04;
				_gpu.state.constantsVertex[g] = Math.cos(angle) * (stage.mouseX - Lib.WIDTH_HALF) - 128;
				_gpu.state.constantsVertex[int(g+1)] = Math.sin(angle) * (stage.mouseY - Lib.HEIGHT_HALF) - 128;
			}
			
			_gpu.backgroundColor.setHex( 0xff000000 | ColorUtils.hsvToRGB(EE.time.now * 0.0001, 1, 1) );
		}
	}
}