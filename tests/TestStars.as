package tests {
	import com.bigp.doublee.EE;
	import com.bigp.Lib;
	import com.bigp.stage3d.GPU;
	import com.bigp.stage3d.GPUAttribute;
	import com.bigp.stage3d.GPUFilter;
	import com.bigp.stage3d.GPUMesh;
	import com.bigp.stage3d.GPUPositionVertices;
	import com.bigp.stage3d.GPUStack;
	import com.bigp.utils.ColorUtils;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class TestStars extends Test_GPU {
		public static const STACK_SIZE:int = 1024;
		public static const TOTAL_STARS:int = 25;
		private var starVertices:GPUPositionVertices;
		private var starIndices:Vector.<int>;
		private var star:GPUMesh;
		private var stars:Vector.<GPUMesh>;
		private var theStack:GPUStack;
		
		protected override function onGPUReady():void {
			super.onGPUReady();
			
			starVertices = new GPUPositionVertices(new Vector.<Number>(20, true));
			
			var starAmplitudes:Array = [100, 300];
			var angleStep:Number = Math.PI / 5;
			var angle = angleStep * .5;
			for (var s:int = 0; s < 10; s++) {
				var id:int = s << 1;
				var amp:Number = starAmplitudes[int(s % 2)];
				starVertices.points[id] = Math.cos(angle) * amp;
				starVertices.points[++id] = Math.sin(angle) * amp;
				angle += angleStep;
			}
			
			starIndices = new <int>[
				0, 1, 2,
				2, 3, 4,
				4, 5, 6,
				6, 7, 8,
				8, 9, 0,
				0, 2, 6,
				0, 6, 8,
				2, 4, 6
			];
			
			theStack = new GPUStack(STACK_SIZE);
			theStack.addAttribute( 2, GPUAttribute.TYPE_04_POSITION_XY );
			theStack.addAttribute( 4, GPUAttribute.TYPE_06_COLOR_MULTIPLIER).setActiveAndDefault( true, 1 );
			theStack.addAttribute( 4, GPUAttribute.TYPE_07_COLOR_OFFSET).setActiveAndDefault( true, 1 );
			theStack.onContextCreated( GPU.context );
			
			stars = new Vector.<GPUMesh>(TOTAL_STARS, true);
			for (var i:int=0, iLen:int=stars.length; i<iLen; i++) {
				star = new GPUMesh();
				star.buildMesh( 10, starIndices, theStack, starVertices );
				stars[i] = star;
				
				GPU.root.addChild(star);
			}
			
			stars[0].x -= 100;
			stars[0].y -= 100;
			//GPU.root.filter = new GPUFilter();
			
			randomizeStars();
		}
		
		protected override function onClick(e:MouseEvent):void {
			randomizeStars();
		}
		
		private function randomizeStars():void {
			for (var s:int = 1; s < stars.length; s++) {
				star = stars[s];
				if(s>0) {
					star.x = Math.random() * Lib.WIDTH - Lib.WIDTH_HALF;
					star.y = Math.random() * Lib.HEIGHT - Lib.HEIGHT_HALF;
					star.rotation = Math.random() * Math.PI * 2;
					star.scaleX = star.scaleY = 0.02 + Math.random() * 0.5;
				}
			}
		}
		
		protected override function onGPUUpdate():void {
			super.onGPUUpdate();
			
			GPU.backgroundColor = 0xff000000 | ColorUtils.hsvToRGB(stars[0].rotation * 0.9 + 90, 1, 0.5);
			
			const DEGREE:Number =  Math.PI / 180;
			
			stars[0].rotation += DEGREE * 1;
			
			var theX:Number = stage.mouseX - Lib.WIDTH_HALF;
			var theY:Number = stage.mouseY - Lib.HEIGHT_HALF;
			
			for (var i:int = 1; i < stars.length; i++) {
				star = stars[i];
				star.rotation += DEGREE * theY * 0.01;
				star.setColor( 0xff000000 | ColorUtils.hsvToRGB(star.rotation * 0.5, 1, 1), false);
				star.setPivot( theX * 0.5, 0);
			}
			
			moreMessage = "VERTS USED: " + theStack.nextAvailableVertexID + "/" + theStack.numVertices;
		}
	}
}