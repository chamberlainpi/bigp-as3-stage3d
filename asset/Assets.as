package asset {

	/**
	 * ...
	 * @author Pierre Chamberlain
	 */
	public class Assets {
		[Embed(source = "atlas.png")]
		public static const ATLAS_PNG:Class;
		
		[Embed(source = "atlas.json", mimeType = "application/octet-stream")]
		public static const ATLAS_JSON:Class;
		
		[Embed(source="level_01.json", mimeType="application/octet-stream")]
		public static const LEVEL_JSON:Class;
		
		[Embed(source = "TEST_IMAGE.png")]
		public static const TEST_IMAGE:Class;
	}
}